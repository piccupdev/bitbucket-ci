FROM node:14.20-alpine

RUN apk add --no-cache \
    autoconf \
    automake \
    libtool

ENV HOME=/home/node
EXPOSE 4000
EXPOSE 5000
EXPOSE 5001
EXPOSE 8080
EXPOSE 8085
EXPOSE 9000
EXPOSE 9005
EXPOSE 9099
RUN apk --no-cache add openjdk11-jre bash && \
    yarn global add firebase-tools@9.10.0 && \
    yarn cache clean && \
    firebase setup:emulators:database && \
    firebase setup:emulators:firestore && \
    firebase setup:emulators:pubsub && \
    firebase -V && \
    java -version && \
    chown -R node:node $HOME
USER node
VOLUME $HOME/.cache
WORKDIR $HOME
CMD ["sh"]
